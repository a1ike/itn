"use strict";

$(document).ready(function () {
  $('.phone').inputmask('+7(999)-999-99-99');
  new Swiper('.i-big', {
    loop: true,
    autoplay: {
      delay: 3000
    },
    navigation: {
      nextEl: '.i-big__next',
      prevEl: '.i-big__prev'
    }
  });
  new Swiper('.i-home-articles .swiper-container', {
    loop: true,
    slidesPerView: 3,
    spaceBetween: 30,
    navigation: {
      nextEl: '.i-home-articles__next',
      prevEl: '.i-home-articles__prev'
    },
    breakpoints: {
      1200: {
        slidesPerView: 1
      }
    }
  });
  new Swiper('.i-service-swiper', {
    loop: true,
    slidesPerView: 4,
    spaceBetween: 30,
    navigation: {
      nextEl: '.i-home-articles__next',
      prevEl: '.i-home-articles__prev'
    },
    breakpoints: {
      1200: {
        slidesPerView: 1
      }
    }
  });
  $('.i-qa__q').click(function () {
    var tab_id = $(this).attr('data-tab');
    $('.i-qa__q').removeClass('current');
    $('.i-qa__a').removeClass('current');
    $(this).addClass('current');
    $('#' + tab_id).addClass('current');
  });
  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.i-modal').toggle();
  });
  $('.i-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.i-modal').toggle();
  });
  $('.i-header__toggle').on('click', function (e) {
    e.preventDefault();
    $('.i-header__nav').slideToggle('fast');
    $('.i-header__info').slideToggle('fast');
  });
  $('.i-questions-card__title').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('i-questions-card__title_active');
    $(this).next().slideToggle('fast');
  });
});
//# sourceMappingURL=main.js.map
